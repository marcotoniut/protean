{-# LANGUAGE NoImplicitPrelude #-}
module Protean.Text.Email.Parser 
  ( module Text.Email.Parser
  , unsafeEmailAddress'
  ) where

import Control.Arrow (second)
import Data.ByteString (ByteString)
import Data.ByteString.Char8 (span, dropWhile)
import Data.Eq ((==), (/=))
import Data.Function ((.))
import Data.Tuple (uncurry)
import Text.Email.Parser (EmailAddress, unsafeEmailAddress)

unsafeEmailAddress' :: ByteString -> EmailAddress
unsafeEmailAddress' = uncurry unsafeEmailAddress . second (dropWhile (== '@')) . span (/= '@')

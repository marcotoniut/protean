{-# LANGUAGE NoImplicitPrelude, InstanceSigs #-}
module Protean.Text.Email.Instances where

import Control.Applicative (pure)
import Control.Monad (fail)
import Data.Aeson (FromJSON, ToJSON, Value(String), parseJSON, toJSON, withText)
import Data.Aeson.Types (Parser)
import Data.Function (($), (.))
import Data.Either (Either(Left, Right))
import Data.Semigroup ((<>))
import Data.Text.Encoding (encodeUtf8, decodeUtf8With)
import Data.Text.Encoding.Error (lenientDecode)
import Text.Email.Parser (EmailAddress, toByteString)
import Text.Email.Validate (validate)

instance ToJSON EmailAddress where
  toJSON :: EmailAddress -> Value
  toJSON = String . decodeUtf8With lenientDecode . toByteString

instance FromJSON EmailAddress where
  parseJSON :: Value -> Parser EmailAddress
  parseJSON 
    = withText "EmailAddress" $ \t ->
        case validate $ encodeUtf8 t of
          Left  err   -> fail $ "Failed to parse email address: " <> err
          Right email -> pure email
  {-# INLINE parseJSON #-}

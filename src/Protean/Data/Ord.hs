{-# LANGUAGE NoImplicitPrelude #-}
module Protean.Data.Ord
  ( module Data.Order
  , (≤), (≥)
  ) where

import Data.Ord

(≤) = (<=)
(≥) = (>=)

(≮) = (≥)
(≯) = (≤)

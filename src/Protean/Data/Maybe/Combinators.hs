{-# LANGUAGE NoImplicitPrelude #-}
module Protean.Data.Maybe.Combinators
  ( filterMaybe
  ) where

import Data.Bool (Bool)
import Data.Maybe (Maybe(Just, Nothing))

filterMaybe :: (a -> Bool) -> a -> Maybe a
filterMaybe f x = if f x then Just x else Nothing

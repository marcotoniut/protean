{-# LANGUAGE NoImplicitPrelude #-}
module Protean.Data.Tuple
  ( module T
  , curry3, uncurry3
  ) where

import qualified Data.Tuple as T

curry3 :: ((a, b, c) -> d) -> a -> b -> c -> d
curry3 f x y z = f (x, y, z)

uncurry3 :: (a -> b -> c -> d) -> (a, b, c) -> d
uncurry3 f (x, y, z) = f x y z

{-# LANGUAGE NoImplicitPrelude #-}
module Protean.Data.Map
  ( module Data.Map
  , mergeWith
  , mergeL
  ) where

import Data.Map (Map, union, intersectionWith, unionWith, map)
import Data.Ord (Ord)

mergeWith :: Ord k => (a -> b -> b) -> Map k a -> Map k b -> Map k b
mergeWith f x y = intersectionWith f x y `union` y

mergeL :: Ord k => (a -> c -> c) -> (b -> c) -> Map k a -> Map k b -> Map k c
mergeL f g x y = mergeWith f x (map g y)

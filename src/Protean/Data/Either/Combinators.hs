{-# LANGUAGE NoImplicitPrelude, ViewPatterns #-}
module Protean.Data.Either.Combinators
  ( maybeToLeft, maybeToRight, fitInEither
  ) where

import Data.Bool (Bool)
import Data.Maybe (Maybe, maybe)
import Data.Either (Either(Left, Right))

fitInEither :: (a -> Bool) -> a -> Either a a
fitInEither f x = if f x then Right x else Left x

maybeToLeft :: b -> Maybe a -> Either a b
maybeToLeft (Right -> r) = maybe r Left

maybeToRight :: b -> Maybe a -> Either b a
maybeToRight (Left -> l) = maybe l Right

swapEither :: Either a b -> Either b a
swapEither (Left  l) = Right l
swapEither (Right r) = Left  r

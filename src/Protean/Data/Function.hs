{-# LANGUAGE NoImplicitPrelude #-}
module Protean.Data.Function
  ( module Data.Function
  , (<..), (..>), (<...), (...>), (.~*)
  ) where

import Data.Function (flip, (.))

infixl 8 <..
(<..) :: (c -> d) -> (a -> b -> c) -> a -> b -> d
(<..) = (.) . (.)

infixl 8 ..>
(..>) :: (a -> b -> c) -> (c -> d) -> a -> b -> d
(..>) = flip (<..)

infixl 8 <...
(<...) = (.) . (.) . (.)

infixl 8 ...>
(...>) = flip (<...)

-- TODO Review the idiom for this composition
infixl 8 .~*
(.~*) :: (c -> d) -> (a -> b -> c) -> b -> a -> d
(.~*) f g = f <.. flip g
